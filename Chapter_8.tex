\documentclass[thesis_0113.tex]{subfiles}
\begin{document}
\chapter{Conclusion}
\chaptermark{Conclusion}
\label{cpt:Con}

\section{Contribution}

This thesis studies the \emph{feasibility} and \emph{advantages} of applying state-of-the-art computer technology in scientific workflow modeling in a collaborative environment. Based on this study, a \emph{reference implementation} for ROA based workflow modelling is proposed, which covers the topics of data management, service management, data and service virtualisation and service composition. This thesis starts with a series of reviews on related researches and technologies related to those four topics. Among them, the data management and web service management forms the foundation of this modelling framework. The study on data and service virtualisation enables the reusability of scientific applications in heterogenous computing environments with various machines/operating systems. The study on web service composition carries out an exploration to further release the potentials of this framework, which enables to describe the composite service declaratively and to execute it by a speculative computing enactment engine. Practically, the contributions of this thesis are achieved from three stages:
\begin{itemize}
\item A light-weight framework for workflow data management and service management that is based on RESTful web service to bridge the gap between desktop scientific application and online scientific workflow;
\item \textcolor{red}{A data and service virtualisation framework to further enhance the efficiency on computing resource and increase the reproducibility of the computing environment for heterogenous scientific applications based on virtualisation/containerization technology;}
\item A speculative enactment engine for scientific workflow, which is based on the light-weight framework.
\end{itemize}


\begin{description}
\item[A Light-Weight Framework for Bridge-Building from Desktop to Cloud]\hfill \\

This study has presented an indication for the benefits arising from our light-weight framework for the deployment and execution of scientific application in the cloud. With our GUI based deployment mechanism, the technical barriers are lowered for non-specialist usage of web services and cloud resources. The framework \textbf {reduces the effort and enhances the efficiency} for users to turn legacy code and programs into web services and hence collaborate with each other. The distributed and asynchronous data staging mechanism helps \textbf {reduce end-to-end times} by hiding the costs of data staging between services as well as between client and service, which works as the foundation to enhance the efficiency of data transfers in the ROA based scientific workflow modelling. Practically, this thesis also evaluates the usefulness and usability of the framework through a user study and some experiments, showing how different types of legacy programs and tools can cooperate seamlessly in workflow with the support of our framework.

\item[A Speculative Enactment Engine for Scientific Workflow]\hfill \\

In Chapter \ref{cpt:DecDes}, a speculative enactment engine is introduced together with the support of the Speculative Service Description Language(SSDL). SSDL is the first declarative workflow description language that is designed for a speculative enactment engine. It further \textcolor{blue}{presents} the capability of declarative description language in the implementation of scientific workflow, which enhances the fault-tolerance ability of scientific workflow. It supports the composition of web services that generates the workflow as autonomous services. Additionally, it utilizes and benefits from the architectural specification of hypermedia distributed systems as described by REST [4], and from the insights and guidelines of ROA [5].\\

On the basis of the SSDL and enactment engine model, this work introduces the prototype implementation of the speculative enactment engine. The implementation brings in the idea of integrating both objective-oriented and logic programming languages to create the web services as the interfaces for the speculative enactment process. This prototype allows the validation of this speculative enactment engine in real application scenarios.
\end{description}

As a summary, this work enhances the efficiency of scientific workflow modelling and presents the feasibility of implementing ROA and RESTful web services in scientific workflows with the introducing of new approaches on data management, service management and service composition in aforementioned two stages. Specifically, this can be observed from two perspectives:
\begin{enumerate}
\item from the application perspective, as a user of scientific workflows, this work validates the feasibility and \textcolor{blue}{demonstrates} the advantages of applying a new ROA based workflow framework.
\item from the development perspective, as a computer scientist, this work fills the gap between new web services technology and scientific workflow modelling by developing a more efficient framework based on ROA and RESTful web services;
\end{enumerate}

\section{Limitations}
\subsection{The Overheads in Web Services based Scientific Workflow}
\label{subsec:chpt7_oh}
In Section \ref{sec:Chpt3_Eva}, Figure \ref{fig:com_1} shows the impact of overheads in network communication. The \textcolor{blue}{fluctuation} of execution times is caused by the varied network situation. It can be assumed that, in workflows with different execution time and I/O data size, this overhead can impact the total performance. Meanwhile, in Section \ref{sec:Chpt5:Eva}, Figure \ref{fig:rt_ratio} also shows the overhead and its trend caused by the introducing of speculative enactment engine. This section will discuss the overhead in web services based scientific workflow and its limitation.

To further investigate the impact of overhead on the performance, first of all, two experiments are set up based on a workflow with
\begin{inparaenum}[(i)]
\item scalable computing time,
\item and transferred data size
\end{inparaenum}
, which are composed based on the framework. It is the sellar workflow script sellar\_MDF\_solver.py in OpenMDAO in this experiment, which is introduced in Chapter \ref{cpt:UCs}. The results are discussed in two parts:

\begin{description}
  \item[Scalable Computing Time] \hfill \\
  The code in sellar.py has a very short compute time (one line of script), which makes it easy to observe the communication costs. With the addition of a line of code to make it “sleep” for a specified period to simulate a longer computing time. The comparison is between the local version of the prolonged script and the web service version of the prolonged script. In them, the web service version has two scenarios that one is deployed on the Amazon EC2 machine, one is deployed on the intranet of UoB.\\

  In this experiment, the workflow is executed for 139 times to get the mean of total execution time. In Figure \ref{pic：scalable_time}, the execution time for three conditions are presented: local workflow, web service workflow (Intranet environment), web service workflow (Internet environment). The x axis represents the sleep time in the script, which can be deemed as the computing time of the original local script. From Figure \ref{pic：scalable_time}, it is not surprising to see generally the execution time of Internet workflow is the slowest and the local workflow is the fastest. We can also observe that the variation in Internet traffic has a significant impact on workflow execution time.\\

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{pic/scalable_time}\\
  \caption{Workflow execution time with different sleep time in three situations (Local workflow, Intranet workflow and Internet workflow)}\label{pic：scalable_time}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{pic/scalable_time_ratio}\\
  \caption{Web service based workflow execution time normalised by the local execution time}\label{pic：scalable_time_ratio}
\end{figure}

  Based on the results from Figure \ref{pic：scalable_time}, we derive Figure \ref{pic：scalable_time_ratio} which plots the ratio between Internet and Local workflow as well as the ratio between Intranet and Local workflow. The latter has a clear downward trend as the computational time grows: from 1.93 at 1s to 1.20 at 5s, and to 1.08 at 10s. In the Internet workflow, we can observe a nonmonotonic trend at the beginning in Figure \ref{pic：scalable_time}, which is simply due to traffic variations in the Internet environment and configuration at server-side. In this experiment, the server is located on the east coast of America. The server is a AWS free-usage tier one, which is allocated with limited and low priority computing and communication resources. However, from Figure \ref{pic：scalable_time}, we can still see the general trend that the ratio between Internet and Local workflow drops from 5.08 at 0.5s to 1.22 at 5s. After 10s, we observe that the differences between the three conditions have stabilized. At 10s, the ratio between Internet workflow and local one reduces to 1.10. \textcolor{blue}{It can be observed the Internet/Local ratio is getting closer or even similar to the Intranet/Local ratio when computing time grows. This indicates that, in the circumstance of that there is only control flow(or the dataflow's size is stable), the influence of network situation on total execution time is getting smaller when computing time of web service grows. After certain level, such as the 5s computing time in this case, the influence on overhead is almost negligible.}

  \item[Scalable Transferred Data Size] \hfill \\
  A further experiment will show that how the execution time scales when large-scale data communication is involved in the workflow. It also aims to observe sensitivity to the increasing of data size.\\

  As a basis, the framework has a stream style communication design for data handling that no matter how large the data is, it is never fully loaded into the memory. The data is divided into a stream of small sections. The memory only loads one section from the hard drive every time and handles the data in a stream, with the objective of reducing the overall memory footprint. Even in some extreme situation when the machine only has very limited memory, it can still deal with large amounts of data. This is also one of our test that will be shown. The user of this framework also does not need to manually configure the memory heap size for the framework (it is based on Java Virtual Machine), the default size is enough.\\

  In this experiment, different sizes of data ranging from 128K to 256M are submitted to web service deployed through the framework. The web service on server side will send back a success response when file is successfully received. The whole execution finishes with a further request to a web service to fetch an HTTP response with empty body. The web service simply simulates the process to consume the data uploaded previously. Two network environments are tested here to provide a comparison. In the first scenario, the service is deployed in an Amazon EC2 machine and the client side is deployed within the University of Bath intranet. In the second, both server and client are within the intranet. Both servers and the client machine have a single core CPU.\\

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.9\textwidth]{pic/scalable_data}\\
  \caption{Experiment results of data communication}\label{pic：scalable_data}
\end{figure}

  To establish a baseline cost for data transfer alone, we ran an experiment using the Secure File Transfer Protocol (SFTP), in order to get a better picture of the influence of the bandwidth and network environment on transmission costs. This data is plotted on Figure \ref{pic：scalable_data} as well. From Figure \ref{pic：scalable_data}, it can be observed that data transmission time rises in line with data size as expected. The useful observation however, once the transfer size outweighs the overhead costs, is that we can observe the overhead for the WS data transfer over SFTP by comparing the blue (WS) and green (SFTP) bars. As usual, small volumes are noisy and uninformative, but above 4Mb, there is a clear differential between WS and SFTP, with the former taking longer. The study here is not sufficiently detailed to be anything more than indicative, but it suggests confirmation of the intuition that data transfer over HTTP incurs a small but notable overhead compared to SFTP, which is tuned for data transfer. Without more detailed investigation it is hard to say whether the factor is additive or multiplicative, but the ratio is falling between 16Mb and 256Mb. The second useful observation, which helps to contextualize these experiments, is that the approximate bandwidth between UoB and the EC2 machine is in the range 0.8-1.0Mb/s. A higher performance network should provide higher bandwidth and hence contribute to a lower performance crossover point in terms of the minimum data size necessary before it is worth using a WS.\\
\end{description}

The limitation can be concluded as,
\begin{inparaenum}[(i)]
\item the performance of data staging is still significantly impacted by the network;
\item the network overhead is a stable factor that, with the growth of computing time and data size, the impact turns smaller;
\item in better network situations, the network overhead takes a smaller part of overall overhead.
\end{inparaenum}

On the other hand, the overhead on data staging is not only limited by network latency. The hardware limitation is yet another influential factor. The mechanical hard disk can reduce the I/O speed signicantly. A possible direction to explore in the future is the adoption of RAM disk. On implementation level, a series solutions can be adopted, such as \textit{ramdiskadm} \citep{ramdiskadm}, \textit{RapidDisk} \citep{rapiddisk}, etc.

\subsection{The Synchronized Connection based on HTTP}

In this framework, the HTTP connections in the workflow are synchronized. That means at network level, for each HTTP connection, it will not be disconnected from server until the end of the computing process. For example, if there is an application that needs a long time of execution, the connection will be maintained as long as the execution.

At client side, the Taverna workflow has a multi-process mechanism in its execution engine. Therefore, the synchronized connection will not block the execution of other available web services invocation. In the Python based and Java based client side, multi-thread code can be involved as well to avoid the impact of synchronization. In both client types, timeout length can be customized. It allows the client to wait for the connection, rather than disconnect it automatically. In a nut shell, in this research work and prototype, the asynchronized connection is not necessary.

In future implementation, the disadvantages of a synchronized connection potentially can be reflected from following aspects:
\begin{inparaenum}[(i)]
\item the client with uncustomisable timeout length, such as some of the browsers, will have a problem if the execution time is too long;
\item the client machine may experience shutdown or restart in this process, or the user wants to retrieve the results from another machine;
\item if there are multiple instances of a connection, to release the resources as soon as possible will also be a positive factor to the system performance.
\end{inparaenum}

The asynchronised connections can be supported by pull or push technology. By implementing pull technology, such as web feeds, the client can maintain an aggregator to subscribe each of the remote executions. Besides, the latest push technology can also provide an option as solution. Such as in HTML5, WebSocket \citep{fette2011websocket} protocol provides the full-duplex communications channels over a single TCP connection. Because it uses TCP port number 80, which can used in the environments that firewalls block non-web Internet connections.

\section{Future Works}
\label{sec:future}

This framework, as an implementation, shows its ability and potential in the application of scientific workflow. As an outcome of this research, it still can have its potentials to be further released in future implementation beyond a prototype. The future works will be carried out from two aspects as well: web services management and speculative enactment engine.
\begin{description}
 \item[Web Service Management] \hfill \\
 The future works on web service management will mainly focus on the following aspects:\\
 \begin{itemize}
 \item In the future, the framework can be further developed to be deployed on more platforms, such as Windows and Mac OS.
 \item The user interface can be migrated from a Client/Server architecture to Browser/Server architecture.
 \end{itemize}
 \item[Speculative Enactment Engine] \hfill \\
 Currently, the speculative enactment engine is used mainly to deal with fault-tolerance or error-handling issues. It has the potential to work as a mechanism to provide alternative solution in more scenarios. For example, when there are multiple options of the inputs for a computing process, it will be helpful to provide a single service that can take multiple input options and generate one optimal result. This can be implemented and tested for some cases in specific research areas. Currently, there is no such a study case in hand in order to build the test workflow and collect the test data. This research and implementation can only be meaningful when there is such a demand to work out the optimal output from multiple inputs. The possible amount of the inputs and the affordable computing resources can potentially influence the efficiency of the system. These will lead to the needs for more case studies and researches. \\
 \textcolor{blue}{On the implementation level, the Prolog based speculative enactment engine can be refactored and written in the OO languages, such as C++, Java because the current solution does not have an ideal link between OO languages and Prolog. For example, JPL does not support multi-process requests. Furthermore, with an OO languages based system, it will reduce the costs to deploy the system with less knowledge preparation and software/library support. Besides, the SSD can be designed and written based on more user-friendly grammar or even align with other description language standards.}
 \item[Integration of Virtualized Scientific Workflow and Speculative Enactment Engine] \hfill \\
 \textcolor{blue}{In this thesis, both of the virtualized scientific workflow platform and the speculative enactment engine are independently developed based on the works introduced in Chapter \ref{cpt:DP} and Chapter \ref{cpt:Mana}. They are both good examples to show the extendibility of the ROA based scientific workflow platform. Furthermore, these two independently developed systems can be integrated together to release more potentials. \\
 For example, now the speculative enactment engine is more like a tool that only serve for client-side users. The whole speculative process can be explicitly programmed and monitored from user's point of view. The whole process is not transparent to users. They are aware of which services are speculatively executed and what alternative services are invoked. By integrating virtualized scientific workflow and speculative enactment engine, this process can be carried out entirely at server-side that users are not aware of there is even a speculative process. The speculative enactment process will be transparent to users. At server-side, for one service, there can be multiple algorithms, multiple instances or multiple inputs that are executed in parallel in a speculative manner to serve for one purpose. They are all controlled by server. From user's point of view, the enactment of this type of \emph{server-side speculative service} does not have difference from a normal service's. Virtual machines can be dynamically started or stopped for each service execution job based on the number of service requests and idle resources. Algorithms can be developed for this dynamic allocation, which needs further research.}
 \item[Future Works on Speculative Enactment Engine] \hfill \\
 \textcolor{blue}{
 In the discussion about time/compute resource decisions in Section \ref{subsec:decision_discussion}, an equation is proposed about how to make the decision. Equation \ref{equa:decision_update_2} is also proposed to further reveal the research potential based on the speculative enactment in scientific workflow.\\
 \begin{equation}\label{equa:decision_update_2}
  D = M_t-S*M_s/(P_s*T_s)
 \end{equation}
 It is discussed that, users or system administrators can adjust their sensitivity to financial cost case by case by setting the $M_t$. This setting can be a static number. It can also be dynamically reflected and configured based on some factors at runtime. Finally, it will lead to a system not only automatically and efficiently, but also economically achieve the workflow tasks in terms of time/compute resource costs. The factor can be the idle resources that left in the HPC system. The coefficient can also be set dynamically based on the change of power price or the daily generated power from the local solar panels. The factors can be more. The data about those factors can be collected from a sensor network. This combined consideration of efficiency and economy will lead to a wide range of future works.}
\end{description}

%\bibliographystyle{unsrt}
%\bibliography{thesis}

\end{document} 