\documentclass[thesis_0113.tex]{subfiles}
\begin{document}
\chapter{Case Studies}
\chaptermark{Case Studies}
\label{cpt:UCs}

This chapter presents case studies on some applications in real scenarios. There are three types of cases:
\begin{inparaenum}[(i)]
\item a workflow for optimization of the wing internal stiffness distribution. This workflow is based on \emph{in-house} code written by non-computer scientist. The workflow is composed in the Taverna.
\item the integration with local program based on MDO frameworks. This workflow is built based on existing MDO frameworks, which do not have the ability to expose themselves as web services and composite web service.
\item a image processing workflow that built based on existing desktop programs.
\end{inparaenum}

\section{Optimization of the Wing Internal Stiffness Distribution}
\label{sec:chpt6_mdo}
\subsection{Scenario}
The optimization process is formed by three main steps: aerogynamics analysis, structural load transfer, structural optimization. In these three steps, there are four programs which are \textit{DoubletLattice, LoadTransfer, FiniteElement and LevelSetOptimizer}. In our services composition case we integrate \textit{FiniteElement} and \textit{LevelSetOptimizer} as one program. The tangible functions of each program are listed below:

\begin{description}
\item[DoubletLattice] \hfill \\
The DoubletLattice program models the aircraft wing as an infinitely thin layer to compute aerodynamic lift and induced drag. The program reads in the input file, aero input.dat that consists of the geometrical definition of the wing, such as span and chord, as well as the flight conditions. By simulating the flow vortices around the wing, the aerodynamic load distribution is extracted to an output file, aero load.dat. This program is written in FORTRAN 77 and FORTRAN 95.
\item[LoadTransfer] \hfill \\
Fluid-structural interaction is an important study when integrating the aerodynamic and the structural discipline. This is because of the two different meshes between aerodynamic (DoubletLattice) and structural (FiniteElement) models. The input script, structural input.dat details the element sizes of the wing which is also going to be modelled in the FiniteElement program. The LoadTransfer program transfers the calculated aerodynamic loads to the finite element mesh via surface spline and computes the structural loads (structural load.dat). The LoadTransfer program is written in FORTRAN 95.
\item[FiniteElement] \hfill \\
The FiniteElement program reads in the structural loads input and calculates the stiffness of the wing structure. The deflection of the wing is computed based on a linear static equation. Therefore, the strain energy, which is the objective function to be minimized, can be calculated. This program is written in C-program.
\item[LevelSetOptimizer] \hfill \\
A level-set method is implemented in C-program to optimize the wing structure. The objective is to minimize the strain energy subject to a volume constraint. During the optimization, the structural boundary is moving inward, hence, the volume is reduced. These involves a number of iterations and in each iteration, the wing deflection and the strain energy are re-calculated as the overall structure stiffness has changed. The solution is converged when the convergence criteria are met, i.e. the targeted volume is achieved and the objective values of the last 5 iterations are less than 0.1
\end{description}

\subsection{Solution}

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.6\textwidth]{pic/mdo_wf}\\
  \caption{The MDO process built in Taverna based on REST web services}\label{pic:chpt4_mdo_wf}
\end{figure}

For the purpose of demonstrating a MDO process in a web services composition, the Taverna workbench is utilized for the jobs of services composition, execution and monitoring with the support of the services located in Datapool and SaaS based framework proposed in Chapter \ref{cpt:DP} and \ref{cpt:Mana}. We demonstrate all the functional modules with an example created in Taverna. Figure \ref{pic:chpt4_mdo_wf} is the screenshot of the services compostion design example, which intends to optimize the topology of the internal layout of an aircraft wing structure. All the dark blue boxes \textit{(GetResults Aerosolve, GetResults AeroLoad transfer, GetResults BLES3)} are RESTful web services built by the web services management system upon three command line programs, which are written in either Fortran or C languages. All the pink boxes \textit{(PutInputs4Aerosolve, PutInputs4AeroLoa transfer, PutInputsBLES3)} are the RESTful web services for the Datapool, whose functions are uploading data as web resources for application services. The input ports built in Taverna based on RESTful web services are located at the top of Figure \ref{pic:chpt4_mdo_wf}, and output ports are located at the bottom. One local service Data Extractor is utilised to retrieve the data based on the URIs collection return by the last application service.

\begin{figure}[!t]
\centering
\hspace*{\fill}
  \subfigure[The initial wing topology]{
    \label{fig:chpt6_topo:a}
    \includegraphics[width=0.3\textwidth]{pic/init_topo}}\hfill
  \subfigure[The final wing topology]{
    \label{fig:chpt6_topo:b}
    \includegraphics[width=0.3\textwidth]{pic/opti_topo}}
    \hspace*{\fill}
  \caption{The wing topology}
  \label{fig:chpt6_topo}
\end{figure}

A simple rectangular wing example is provided and tested by the non-specialist to demonstrate the feasibility of this ROA-based framework. In the aero input.dat, a wing structure of 20 strips and 5 chord boxes with a $0\textdegree$ dihedral angle and $4\textdegree$ incidence is defined. The wings root chord is 8 meters and span is 20 meters. Meanwhile, the ratio of root chord and tip chord is 1. The flight condition is set as 0.7 MACH and 35000 feet. After the execution of aerodynamics analysis and load transfer steps, the structural loads that are applied on each finite element nodes are interpolated from aerodynamic loads using spline method. By using the structural loads transferred from aerodynamics loads as input, final topology solution is generated by the structural optimization step. Figure \ref{fig:chpt6_topo} depicts the initial topology and final optimum solution. The solution is generated based on the numerical result from PlotShapeFinal output port.

\section{The Integration with local program based MDO frameworks}

\subsection{Scenario}

The framework also has the ability to integrate with existing MDO frameworks. As reviewed in Chapter \ref{cpt:LR}, there are some frameworks already include the web service function or a server/client architecture. However, some frameworks that can only be executed as local programs. A learning curve is needed for the non-specialists to fill the gap between those local frameworks and online service based functions. In this case, the ability to deploy legacy MDO workflows based on existing MDO frameworks like OpenMDAO\citep{openMDAO} and Dakota\citep{dakota} is presented.

With the support of the OpenMDAO runtime installed in the server, the deployment process can be achieved as easily as for any other command-line program. On the other hand, Dakota has a different execution approach in that the workflow is defined as a input file, which is then executed by the Dakota runtime. With the Dakota runtime installed in server, the workflow can be executed as a web service by simply uploading the input file through the Datapool service.

\subsection{Solution}

\begin{description}
\item[openMDAO Service] \hfill \\
The executable based on the openMDAO framework is a python script, which is created by users. The openMDAO works as a python library to support the functionality of the MDO workflow. In this case a Multidisciplinary Design Feasible (MDF) workflow \citep{openmdao_mdf} is deployed as a RESTful web service based on the deployment GUI tool and the Datapool service. In this case, the workflow executables have to be deployed to the server as a web service. If other MDO workflows need to be deployed, the corresponding python script should be deployed in advance as web services through the GUI tool as well. The generated web service description is presented in Figure \ref{pic:chpt6_mdf}.

\begin{figure}[!t]
\begin{verbatim}
<WS_Definition>
<Service_Name ifPublic="no">sellar_MDF</Service_Name>
<Application_Name IfDakota="no"
  IfExecutableJar="no"
  IfOpenMDAO="yes"
  IfPythonModule="no">
   sellar_MDF_solver.py</Application_Name>
<Inputs>
<Input IsValue="yes"
  NotInArgs="no" name="z1" qualifier="-zi"/>
<Input IsValue="yes"
  NotInArgs="no" name="z2" qualifier="-z2"/>
<Input IsValue="yes"
  NotInArgs="no" name="x1" qualifier="-x1"/>
</Inputs>
<STDOut name="sellar_result"/>
</WS_Definition>
\end{verbatim}
\caption{The description file for openMDAO MDF service}
\label{pic:chpt6_mdf}
\end{figure}

In the description in Figure \ref{pic:chpt6_mdf}, the three parameters of the optimizer are configured as inputs. The output stream from the openMDAO framework is configured as the output of the web service. They are all stored in the Datapool service and accessible through allocated URIs.

\item[Dakota Service] \hfill \\

The Dakota has a different architecture as the openMDAO. It is a software package that reads in an \textit{.in} as a description to the MDO　problem. The user writes the description rather than a script to solve the problem as what the openMDAO directs users to do. Thus, to deploy this framework as a web service, first it is assumed the Dakota software is already installed in the server. The generated description of the RESTful web service is presented in Figure \ref{pic:chpt6_dakota}. \textcolor{blue}{ This description is generated by the GUI based interface introduced in Section \ref{subsec:chpt4_gui}. Through the GUI interface, the command-line entry for this software can be deployed as web service on server side. Then, to solve a specific problem, user just needs to send the problem description \textit{.in} input as he/she does for the desktop version of Dakota software to the web service.} In this case, a problem called \textit{rosenbrock} is used as an example, which is extracted from its User Manual \citep{eldred2007dakota}.

\begin{figure}[!t]
\begin{verbatim}
<WS_Definition>
<Service_Name ifPublic="yes">dakota_t1</Service_Name>
<Application_Name IfDakota="yes"
  IfExecutableJar="no"
  IfOpenMDAO="no"
  IfPythonModule="no"/>
<Inputs>
<Input IsValue="no" NotInArgs="no"
  name="dakota_rosenbrock_2d_in" qualifier="-i"/>
</Inputs>
<Outputs>
<Output IsValue="no" NotInArgs="no"
  name="out_2d" qualifier="-o"/>
</Outputs>
</WS_Definition>
\end{verbatim}
\caption{The description file for Dakota service}
\label{pic:chpt6_dakota}
\end{figure}

\textcolor{red}{In Figure \ref{pic:chpt6_dakota}, the necessary information to deploy the Dakota script as web service is described for the whole framework.} In it, the input is the \textit{.in} file. The output of the web service is the output file from the Dakota framework. It can be noticed that, in this case, the service description does not have an application name, because the application that this web service aims to wrap has been deployed at the server side. The problem itself is not an executable script or program. In other words, all the Dakota related service actually have the same application name.

\end{description}

\section{Image Processing Workflow}

\subsection{Scenario}
PovRay\citep{povray} is a ray tracing program which generates images from a text-based scene description, which is written in the POV description language. ImageMagick\citep{imagemagick} is a software suite to create, edit, compose, or convert images. Conventionally, the user needs to manually operate these two different pieces of software to achieve a workflow, such as generating an image by following an editing step on the image. Surely, there is learning curve for both of the software.

\begin{figure}
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[width=0.7\textwidth]{pic/imgWF.eps}\\
  \caption{The image processing workflow}\label{pic:chpt6_imgwf}
\end{figure}


In this case, a workflow, as shown in Figure \ref{pic:chpt6_imgwf} based on web services and the ROA framework will be created to generate image from a POV file and then convert the 3-D image from the \textit{png} format to the \textit{jpeg} format. Both of the command-line executables are written as shell scripts. The PovRay executable also has files reside that serve as libraries for 3-D image generation. Both of the PovRay and ImageMagick have been installed in the target server.

\subsection{Solution}

The related executables and libraries for each program are packed as two zip files. They are deployed through the GUI tool as RESTful web services. The generated web service description files are listed in Figure \ref{pic:chpt6_povser} and \ref{pic:chpt6_imser}.

\begin{figure}[!t]
\begin{verbatim}
<WS_Definition>
<Service_Name ifPublic="yes">Povray_House</Service_Name>
<Application_Name
  IfDakota="no" IfExecutableJar="no"
  IfOpenMDAO="no" IfPythonModule="no">
    povray.sh</Application_Name>
<Inputs>
<Input IsValue="no" NotInArgs="no" name="pov"
  qualifier=""/>
</Inputs>
<Outputs>
<Output IsValue="no" NotInArgs="no" name="png"
  qualifier=""/>
</Outputs>
</WS_Definition>
\end{verbatim}

\caption{The description file for PovRay service}
\label{pic:chpt6_povser}
\end{figure}

\begin{figure}[!t]
\begin{verbatim}
<WS_Definition>
<Service_Name ifPublic="yes">Convert</Service_Name>
<Application_Name IfDakota="no" IfExecutableJar="no"
  IfOpenMDAO="no" IfPythonModule="no">
    convert.sh</Application_Name>
<Inputs>
<Input IsValue="no" NotInArgs="no" name="png"
  qualifier=""/>
</Inputs>
<Outputs>
<Output IsValue="no" NotInArgs="no" name="result_jpeg"
  qualifier=""/>
</Outputs>
</WS_Definition>
\end{verbatim}

\caption{The description file for ImageMagick service}
\label{pic:chpt6_imser}
\end{figure}

The workflow also contains two Datapool services. The first Datapool service is used for initial input data uploading. The second one is the one to store intermediate data, the \textit{png} file. The Datapool service for ImageMagick receives this \textit{png} file as a URI reference (step 6 in Figure \ref{fig:execution}). They are invoked from the client-side by an executable script written in Python, which supports the invocation of RESTful web services. One syntax example is shown in Figure \ref{pic:chpt4_python} on Page \pageref{pic:chpt4_python}.

%\bibliographystyle{unsrt}
%\bibliography{thesis}

\end{document}
