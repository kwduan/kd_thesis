\documentclass[thesis_0113.tex]{subfiles}
\begin{document}
\chapter{Datapool: A ROA Based Distributed Data-flow Model}
\chaptermark{Datapool}
\label{cpt:DP}

\section{Introduction}
\label{sec:DP:Intro}

The problem of staging data in workflows has received much attention over the last decade, with a variety of user-directed and automatic solutions. \textcolor{blue}{In Section \ref{subsec:datastaging}, the existing solutions and researches are introduced. A trend of distributed management of data and separation of data-flow and control-flow emerges, which aims to resolve the performance overhead when the two flows are embedded with each other. The first challenge in the design for this ROA based WMS emerges as, how to design a data staging mechanism that can utilise the light-weight feature, follow the constraints of ROA and also enable the data staging in a distributed manner with separation of data-flow and control-flow}. This chapter presents a ROA based distributed data-flow model that aims to improve data staging performance. This data-flow model has following features\kdnote{The first paragraph is partly rewritten to make a realistic claim and concrete description}:
\begin{enumerate}
\item The model is designed based on mature and widely applied technology. It does not use a private~-- by which we mean internal, or closed~-- mechanisms (functions are exposed by a set of developer defined specific interfaces and operations) to handle data transfer.
\item The model supports universal access through \textcolor{blue}{network} to data objects, which are viewed as online resources. By this, this mechanism enables the access to provenance data.
\item The model provides a seamless interface for application services to access individual data objects or a collection of data objects.
\item In this model, data-flow is separated from control-flow to avoid central coordination, which forms the basis for distributed data transfers.
\item It supports a distributed data staging mechanism. Data-flow can be distributed among services without passing through a central controller.
\item It supports asynchronous data staging. Data transfers can be initiated asynchronously before the actual execution of a service when data comes from different sources.
\end{enumerate}

The rest of this chapter will first provide a detailed design of this model as well as its comparative advantages
in Section \ref{sec:DP:P2PDF}; then implementation of this model based on ROA in Section \ref{sec:impl}; evaluation of performance is provided in Section \ref{sec:Chpt3_Eva}; there will be a summary in Section \ref{sec:chpt3_sum}. Furthermore, there is a discussion of limitations in Section \ref{subsec:chpt7_oh}.

\section{Distributed Data-flow}
\label{sec:DP:P2PDF}
Inspired by the principle of distributed data transfer, this thesis presents a data-flow model for web services composition, which is able to alleviate the communication bottleneck between client and server. There are two quite obvious remarks about data-flows among multiple services: (i) for a given service invocation, the data-flow rarely involves the client or central controller, which means that data-flows can (normally) be distributed, and (ii) it is not uncommon that the necessary data objects (inputs) may come from different sources, suggesting that data transfers can be initiated asynchronously before the actual execution of a service. These constitute the two properties that our data staging mechanism needs to satisfy.

\subsection{Distributed Data Staging}
\label{subsec:DP:P2PDF:DDS}

\begin{figure}[!t]%[!h]
   \centering
   \begin{minipage}[t]{0.80\textwidth}
      \vspace{5pt}\raggedright
      \includegraphics[width=\linewidth]{pic/UDDF_1}
      \caption{Centralized Data-Flows in Web Services Composition}
      \label{fig:UDDF_1}
   \end{minipage}
   \newline
   \begin{minipage}[t]{0.80\textwidth}
      \vspace{20pt}\raggedright
      \includegraphics[width=\linewidth]{pic/UDDF_2}
      \caption{Distributed Data-flows in Web Services Composition}
      \label{fig:UDDF_2}
   \end{minipage}
\end{figure}

Figures \ref{fig:UDDF_1} and \ref{fig:UDDF_2} illustrate the essential difference between a centralized and a distributed mechanism for data transfer. Figure \ref{fig:UDDF_1} shows that both control-flow and data-flow are centrally coordinated for each web service invocation. There is a high risk that the client or central controller becomes a bottleneck for data communication among computation components. In Figure \ref{fig:UDDF_2}, the data-flows are distributed among web services directly rather than passing through a central controller, which also allows for the concurrent transfer of data objects from different resources. The I/O data communication carries out in network between each related web service. The client can also obtain the complete set of data objects from the network whenever it needs. Hence, each service provider takes care of the task of data storage instead of the client.

This comparison also illustrates the additional technical requirements on the distributed data staging mechanism. Two key functions are identified:
\begin{inparaenum}[(i)]
\item an universal addressing system that allows each individual data object can be identified and accessed in network,
\item an server-side data management service that allows data object to be stored, grouped and accessed based on that universal address system.
\end{inparaenum}
Chapter \ref{cpt:LR} showed that some other distributed data transfer system to achieve these two requirements either through the use of an extra layer of private protocols, above the Internet application layer protocols, or the introduction of a potentially complicated (internal) system to achieve distributed data transfer. This thesis aims to present that an open solution can be achieved by simpler web services based architecture like ROA and in which data objects can be addressed just by URIs. More design details are presented in Section \ref{sec:impl}.


\subsection{Asynchronous Data Staging}
\label{subsec:DP:P2PDF:ADS}

As aforementioned, it is common that necessary data objects come from different sources. In a centralized way, control-flow and data-flow are centralized coordinated through client. Therefore, the inputs from different sources for next service have to be synchronized at client in advance. In an arbitrary web service, it is common to have multiple data objects compacted into one data object, such as XML, JSON file, which naturally makes the data staging among services working in a synchronous style. On the other hand, in REST-compliant web services, it is also common phenomenon that multiple data objects are compacted together into a data structure, which is either contains in URI or HTTP body data.

\begin{figure}[t!]
\centering
\includegraphics[width=0.8\textwidth]{pic/timeline}
\caption{Comparison of two different data staging mechanisms}\label{fig:timeline}
\end{figure}

The comparison between asynchronous and synchronous data staging mechanisms are demonstrated in Figure \ref{fig:timeline}, where there are three data objects from three different web services that need to be transferred to another service as inputs through three different connections. We make the not entirely realistic assumption that in the two situations, the same data object transfer takes the same time. Under synchronous data transfer, because the data references are controlled through the client, data transfer only starts when the last service finishes and the next service invocation happens. However, in the asynchronous method, the transfers start asynchronously when each previous service finishes. The transfers are not synchronized with the invocation of next service. Data objects will be transferred in advance. From Figure \ref{fig:timeline}, we can clearly see that the asynchronous method may be able to bring about an earlier completion of the whole data transfer process.

\begin{figure}[t!]
\centering
\includegraphics[width=0.5\textwidth]{pic/asyn_demonstration}
\caption{The data dependency in a simple example of multiple steps of workflow}\label{fig:asyn_demo}
\end{figure}

The advantages of asynchronous data staging are mainly embodied in the enactment of workflow, such as multiple steps in sequence. This is shown in Figure \ref{fig:asyn_demo} by a simple workflow data staging example. In this example, there is a data dependency that Service 3 depends on both Service 1 and Service 2 directly. D2 is generated after Service 1's execution. In synchronous data staging mechanism, D2 has to wait for Service 2's execution and is submitted to Service 3 together with D4. In asynchronous mechanism, D2 can be transferred to Service 3 in advance in spite of the execution of Service 2. Obviously, with the increase of number of sequential steps in workflow, there are higher possibility that the overhead on data staging can be saved by asynchronous mechanism.

\section{The Implementation of Distributed Data-flow Model}
\label{sec:impl}

In order to implement the distributed data-flow model in this framework, there are several basic design requirements to be considered. First of all, the addressability of data objects. By following the design principles of ROA and the URI protocol, we are straightforwardly able to achieve the requirement of enabling data objects being identified and accessed universally on Internet. Secondly, it is also required for users to be able to deploy services that can be joined up via this implementation, without the need to take any special action or need to modify existing applications. Finally, it is also a requirement that an appropriately authorized user should be able to manipulate only their own data objects, in order both to secure a given workflow's data, but also to avoid unintentional conflicts with other users.

\subsection{Datapool Service}

The Datapool service is the RESTful web service for I/O data items manipulation (uploading, retrieval, etc.). Datapools are components that used to connect data with application services, which implement the distributed data-flow model. Each Datapool represents a collection of data items each with a unique URI. Each data item inside one Datapool is allocated with unique URI to enable \emph{addressability} as well.

There are two advantages to organising data in this way. First, because all the data items and data collections are directly allocated with URIs, they are all web resources that can be re-accessed through the HTTP protocol at any time rather than merely a data stream in the form of an extra layer of XML or other structure. Furthermore, each data item can also be transferred and kept in their original textual or binary format. Second, in the execution of an application service, the URI of one Datapool that contains all the input data is provided to the service. The application will pull the necessary data automatically from the provided local or remote Datapool. In this way, the interfaces are unified for different application services in the form of one URI, of which the Datapool URI is a constituent as a query string.

Distribution of data between services is greatly simplified through the use of URIs. Each Datapool service is collocated with application services in the same host as shown in Figure \ref{fig:UDDF_2}. Hence the basic function of the Datapool is to provide data storage for association with a particular service, whereby data objects are uploaded into a particular Datapool, such as during initialization, or are transferred from one Datapool to another as a result of the control-flow. In each case, the object will be given a name derived from the Datapool's URI.

Asynchronous data staging is also achieved by the existing of Datapool. We identify that the essence of asynchronous data staging is the separation of control-flow and data-flow. In our case, data-flows among services can be totally controlled through Datapool service. Control-flow merely needs to pass through the application services that associated with the Datapool service which is collocated in the same host. Each data object can be submitted to or retrieved from Datapool service independently from control-flow.

\subsection{User Interface}

For the purpose of enabling data being transferred from client to Datapool or between two Datapools, firstly, it exposes an interface for users to inject documents of any format into the repository as either textual data or arbitrary binary data. Datapool service automatically allocate URIs and organize them in the form of a collection of URIs. Secondly, clients can submit the URI of the data object to Datapool. Datapool will automatically obtain and allocate a new URI to it. Afterwards, before the execution of service, client merely need to provide the URI of the corresponding local Datapool to web service, which contains all the necessary inputs. Web services will automatically extract and consume the data it needs based on data object's name. The whole interface is built based on the HTTP protocol to allow the \emph{uniform interface} of ROA.

\begin{table}[!t]
\centering\resizebox{0.98\textwidth}{!}{
\begin{tabular}{c|c|l}\hline
& Methods & URIs \\\hline
& PUT & http://\ldots/datapool/\{Datapool\_Instance\_Name\}/\{Data\_Object\_Name\}\\
& PUT & http://\ldots/datapool/\{Datapool\_Instance\_Name\}?DO\_URI=\{Data\_Object\_URI\}\\
Datapool&GET & http://\ldots/datapool/\{Datapool\_Instance\_Name\}/\{Data\_Object\_Name\}\\
Services& GET & http://\ldots/datapool/\{Datapool\_Instance\_Name\}\\
& DELETE & http://\ldots/datapool/\{Datapool\_Instance\_Name\}/\{Data\_Object\_Name\}\\
& DELETE & http://\ldots/datapool/\{Datapool\_Instance\_Name\}\\
\hline
\end{tabular}}
\caption{URIs of Datapool Service}\label{table:uris_dp}
\end{table}

The services of Datapool are accessible through URIs as shown in Table \ref{table:uris_dp}. Users can maintain several Datapool instances for different processes or for different stages in one process, and only the owner of the Datapool instance can control the data objects stored in it and decide who can access them, which provides a degree of security for user data. Data resources in the Datapool instance have unique URIs so that different users cannot unintentionally conflict each other.

The HTTP methods - GET, PUT, DELETE - are used in these URIs to represent corresponding functions of the Datapool service. The Datapool service aims to provide following functions:
\begin{inparaenum}[(i)]
\item \emph{create}, \emph{update}, \emph{retrieve}, \emph{delete} of Datapool instances,
\item \emph{create}, \emph{update}, \emph{retrieve}, \emph{delete} of data object in Datapool instance.
\end{inparaenum}
The number of functions can be further reduced to simplify the method vocabulary. The \emph{update} job of Datapool instance actually equals the \emph{create}, \emph{update} and \emph{delete} jobs of data objects. The \emph{create} job of Datapool instance is automatically activated by the \emph{create} job of data object in a new Datapool instance. There is no necessity to create an empty Datapool instance. Meanwhile, the \emph{create} job of data object can share the same method of \emph{update}. The Datapool system can distinguish them by checking if the requested data object is existing. Therefore, there are five jobs that needed to be achieved by the those URIs and HTTP based methods.
\begin{inparaenum}[(i)]
\item \emph{retrieve}, \emph{delete} of Datapool instance,
\item \emph{update}, \emph{retrieve}, \emph{delete} of data object in Datapool instance.
\end{inparaenum}

The first two PUT methods in Table \ref{table:uris_dp} are used for \emph{update} job of data object. First is used for uploading raw data directly. Second is used to upload from another Datapool by providing the URI of data object. The first GET method in Table \ref{table:uris_dp} is used to retrieve the raw data of the data object. The second GET method is used to retrieve the list of the data object URIs in the retrieved Datapool instance. The last two DELETE methods are used to delete the corresponding data object or the whole Datapool instance.

\subsection{\textcolor{blue}{Usages and Technical Details}}
\begin{description}
  \item[Datapool Service] Datapool service is deployed in each of the machine where applications reside in. This Datapool service is always associated with the data staging activities of the applications in the machine it is deployed. When there are multiple machines, there will be same number of Datapool services deployed on those machines, one for each. Cross-machine data staging happens between two Datapool services in two different machines. In Table \ref{table:uris_dp}, the URI prefix \emph{http://\ldots/datapool/} represents Datapool Service. Datapool Service can contain multiple Datapool instance, where the data object are categorised based on their usages and owners. At this level, only the system admin have the permission to do configuration and manipulation. In the OS file system of the Datapool service, their will be a designated folder to hold all the data instances as sub-folders. The path of the designated folder can be customised in the configuration file for the Datapool service.
  \item[Datapool Instance] Datapool instance is the data collection in Datapool service. There can be multiple Datapool instances in one Datapool instance. Each of the Datapool instances can have associated data owner or group. Only the owner or the member in the group has the access to the data objects in the corresponding Datapool instance. Contrarily, one owner or one group can have multiple Datapool instances in one Datapool service for different usages. For example, Datapool instance X can be used only for Service X's data staging activities and instance Y is only used for Service Y. In Table \ref{table:uris_dp}, the URI prefix \emph{http://\ldots/datapool/\{Datapool\_Instance\_Name\}} represents the Datapool instance. Each of them can have it own unique name. As mentioned, each Datapool instance appears as sub-folder in the designated folder for Datapool Service in OS file system. Their folder name is the same as the \emph{Datapool\_Instance\_Name} appeared in the URI.
  \item[Data Object in Datapool Instance] Data object is the real raw I/O data from applications. Their accessibility is decided by the Datapool instance where they are put in. Only the Datapool instance owner or owner group can access the data objects. In the OS file system, they are saved in their raw formats in each of the corresponding Datapool instance sub-folder. When there is data staging activities happen between two Datapool instance in the same machine, the real background activity in the OS is just to change the original path of the file to let it points into the new Datapool instance sub-folder. If it is cross-machine data transfer, the original data will be deleted, and a new file will be created in the new sub-folder in the other machine OS. In Table \ref{table:uris_dp}, \emph{Data\_Object\_Name} in one complete URI \emph{http://\ldots/datapool/\{Datapool\_Instance\_Name\}/\{Data\_Object\_Name\}} means the name of the Data object. It is also the file's name in the Datapool instance's sub-folder.
\end{description}

\subsection{Authentication and Authorization}

Authentication and authorization are key functions that should be applied to the Datapool system. One of the important reasons of applying ROA is that RESTful web service enforces security based on the HTTP/HTTPS protocol. There is no necessity to implement an extra security layer for such purpose. Based on that, an authorization system is implemented to enable a role-based control authorization. There are two obvious benefits brought by it: firstly, developers do not need to design and embed a new security mechanism into the system, which can save developing time and reduce knowledge preparation; secondly, users also do not need to extra knowledge to get familiar with the security mechanism as well as do not need to install an extra layer of software or plug-ins to use it, because of the wide application of the HTTP/HTTPS protocol.

The HTTPS protocol is a communications protocol for secure communication over a computer network. It simply layers the HTTP protocol on top of the SSL/TLS protocol, thus adding the security capabilities of SSL/TLS to standard HTTP communications. By applying HTTPS, the data object that is transferred among services and between service and client can be encrypted and protected. This does not lay any extra burden to users. The only difference from the point of view of operation is that the URI used for data manipulation starts with \emph{https} rather than \emph{http}. Table \ref{table:uris_https_dp} shows two examples that used to PUT and GET data object.

\begin{table}[!t]
\centering\resizebox{0.98\textwidth}{!}{
\begin{tabular}{c|c|l}\hline
& Methods & URIs \\\hline
Datapool& PUT & https://\ldots/datapool/\{Datapool\_Name\}/\{Data\_Object\_Name\}\\
Services& GET & https://\ldots/datapool/\{Datapool\_Name\}/\{Data\_Object\_Name\}\\
\hline
\end{tabular}}
\caption{Secure URIs of Datapool Service}\label{table:uris_https_dp}
\end{table}

Multiple Datapool instances can be generated and customized through the Datapool service by multiple users. Necessary authorization policies are applied to Datapool service to regulate the operations of users. The user of this framework, which includes Datapool service, is enforced to register a pair of username and password. The principle of the policies is only the creator the of Datapool instance \textcolor{blue}{or anyone who is given the authorization} by the creator has the access to it. This is applied to all the operations: \emph{update}, \emph{delete}, \emph{retrieve}. For example, in the case of the second URI is invoked for data object uploading in Table \ref{table:uris_dp}. The user has to make sure that he/she does not only have the access to the Datapool instance where the data object is uploaded to, but also the access to the other Datapool where the data object is stored.

\section{Evaluation}
\label{sec:Chpt3_Eva}

In order to evaluate the performance of services deployed using the new framework, a wing optimization process workflow, which is written in Taverna, (more details about this workflow are introduced in Section \ref{sec:chpt6_mdo}) is executed in two network-based ways:
\begin{inparaenum}[(i)]
\item with all the programs deployed as SOAP services and controlled through a centralized client, including all the data transfers, constituting in effect a worst case scenario for transfer overheads, and
\item with the programs deployed as REST services, using a centralized client for control, but the distributed data-flow model for data.
\end{inparaenum}
Firstly, these two modes are compared, where the programs or services are executed in the same machine environment and the network environment is the same.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{pic/time}\\
  \caption{Comparison of 1000 continuous executions}\label{fig:com_1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{pic/comp}\\
  \caption{Results of simple workflows work with both Centralized and Distributed
Data-flows}\label{fig:com_2}
\end{figure}

To provide preliminary evidence that the REST web services with distributed data-flows performs better than the centralized approach, we ran an experiment of 1000 consecutive executions for both processes in the same environment with workflows written in Taverna. The result is presented in Figure \ref{fig:com_1}\footnote{The x-axis only denotes the number of the run: it does not signify concurrent execution of the two modes.  The data from the two sets of runs is overlaid to facilitate comparison of the execution times.}. \textcolor{blue}{There is fluctuation caused by the real world scenario,} but the figure shows that the REST workflow is faster by a clear margin. \textcolor{blue}{In order to show a comparison between centralized method and distributed method in the same situation, we wrote a workflow in Taverna based only on RESTful services that just moves data from client to one service, on to another, then back to the client.} These two services are deployed in two different VMs on the same LAN as the client. Client and servers access each other by URIs. We set up two scenarios both using RESTful services, but while one uses centralized transfer, the other uses the distributed method. In first scenario, the data transferred from the first service to the second is included in the HTTP body, while in the second just the URIs are transferred and data is transferred in the background by Datapool. The results are shown by Figure~\ref{fig:com_2}. Each workflow was run 10 times for the two scenarios and different data sizes to obtain the mean value. The results suggest an expected trend, in that gains increase with the size of data to be transferred.

\section{Summary}
\label{sec:chpt3_sum}
This Chapter presents a systematic mechanism for a ROA based distributed data-flow model. It describes the design and implementation of a distributed data-staging mechanism, that is itself RESTful, by following REST design principles for the support of data-intensive (RESTful) workflows. It is evaluated by means of an engineering workflow developed for multi-disciplinary design optimization. The workflow is specified in Taverna, which is a conventional centralized data-staging enactment system. However, by virtue of the underlying services and staging mechanisms described here, the resulting enactment is distributed, which furthermore permits asynchronous staging, with \kdnote{potential is deleted} benefits for network utilization and end-to-end execution time. \textcolor{blue}{Therefore, the contributions of this work can be summarised as:
\begin{itemize}
  \item It provides a data staging mechanism for ROA based distributed system that it utilises the features and follows the design constraints of RESTful web service without introducing an extra layer of protocol.
  \item This data staging mechanism has the ability to separate the data-flow and control-flow in workflow enactment. By having the asynchronous feature, it is also able to further separate the data-flow based on the data source and data destination. By comparing with the workflow with combined data-flow and control-flow, this mechanism can provide better data staging performance by its distributed and asynchronous features.
\end{itemize}}

%\bibliographystyle{unsrt}
%\bibliography{thesis}

\end{document}
